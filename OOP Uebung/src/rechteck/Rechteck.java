package rechteck;


public class Rechteck {
	
	private double lengthA;
	private double lengthB;
	
	public Rechteck(double lengthA, double lengthB) {
		setLengthA(lengthA);
		setLengthB(lengthB);
	}
	public void setLengthA(double lengthA) {
		this.lengthA = lengthA; 
	}
	
	public double getLengthA() {
		return lengthA;
	}
	public void setLengthB(double lengthB) {
		this.lengthB = lengthB; 
	}
	
	public double getLengthB() {
		return lengthB;
	}
	
	public double getFlaeche() {
		return (this.lengthA * this.lengthB);
	}
	
	public double getUmfang() {
		return ((2 * this.lengthA) + (2* this.lengthB));
	}

	
}

