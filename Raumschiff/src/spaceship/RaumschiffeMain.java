package spaceship;
/**
 * die Klasse in der Raumschiff und Ladung getested wird
 * @author ALJETKE
 *
 */
public class RaumschiffeMain {
	/**
	 * main methode die das Programm ausf�hrt
	 * @param args
	 */
	public static void main(String[]args) {
		
		Ladung saft= new Ladung("Ferengi Schneckensaft", 200);
		Ladung borgSchrott= new Ladung("Borg-Schrott", 5);
		Ladung roteMaterie= new Ladung("Rote Materie", 2);
		Ladung forschungssonde= new Ladung("Forschungssonde", 35);
		Ladung torpedoes= new Ladung("Photonentorpedo", 3);
		Ladung plasmaWaffe= new Ladung("Plasma-Waffe", 50);
		Ladung schwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		
		
		
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Kharzara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 50, 80, 100, 5, "Ni'Var");
	
		klingonen.setLadungRaumschiff(saft);
		klingonen.setLadungRaumschiff(schwert);
	
		romulaner.setLadungRaumschiff(borgSchrott);
		romulaner.setLadungRaumschiff(roteMaterie);
		romulaner.setLadungRaumschiff(plasmaWaffe);
	
		vulkanier.setLadungRaumschiff(forschungssonde);
		vulkanier.setLadungRaumschiff(torpedoes);
		
		klingonen.feuerTorpedoes(romulaner);
		romulaner.feuerPhaser(klingonen);
		romulaner.nachrichtAnAlle("Gewalt ist nicht logisch");
		
		klingonen.statusRaumschiff(); 
		klingonen.statusLadung();
		
		vulkanier.repariereRaumschiff(true, true, true, 5);
		vulkanier.setAnzahlTorpedoes(torpedoes.getAnzahl());
		vulkanier.leereLadung(torpedoes);
		
		klingonen.feuerTorpedoes(romulaner);
		klingonen.feuerTorpedoes(romulaner);
		
		klingonen.statusLadung();
		klingonen.statusRaumschiff();
		
		romulaner.statusLadung();
		romulaner.statusRaumschiff();
		
		vulkanier.statusLadung();
		vulkanier.statusRaumschiff();
	}
}
