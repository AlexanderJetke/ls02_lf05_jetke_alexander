package spaceship;
/**
 * eine Klasse die Ladung eines Raumschiffes darstellt.
 * @author JETKE
 *
 */
public class Ladung {

	private String bezeichnung;
	private int anzahl;

	/**
	 * standart Konstruktor
	 */
	public Ladung() {

	}
	/**
	 * überschriebender Konstruktor
	 * @param bezeichnung
	 * @param anzahl
	 */
	public Ladung(String bezeichnung, int anzahl) {
		setBezeichnung(bezeichnung);
		setAnzahl(anzahl);
	}
	/**
	 * gibt die Bezeichnung der Ladung zurück
	 * @return
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}
	/**
	 * setzt den Parameter als Bezeichnung der Ladung mit dem Parameter
	 * @param name
	 */
	public void setBezeichnung(String name) {
		this.bezeichnung = name;
	}
	/**
	 * gibt die Anzahl einer Ladung zurück
	 * @return
	 */
	public int getAnzahl() {
		return anzahl;
	}
	/**
	 * setzt die Anzahl einer Ladung fest mit dem Parameter
	 * @param anzahl
	 */
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	/**
	 * diese Methode überschreibt die standart toString() methode
	 */
	@Override
	public String toString() {
		return "Bezeichnung=" + bezeichnung + ", anzahl=" + anzahl;
	}
	/**
	 * leert eine Ladung
	 */
	public void leereLadung() {
		this.bezeichnung =null;
		this.anzahl=0; 
	}
}
