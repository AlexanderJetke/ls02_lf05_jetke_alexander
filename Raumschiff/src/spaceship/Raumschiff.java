package spaceship;
import java.util.ArrayList;
/**
 * eine Klasse die ein Raumschiff abbildet mit seinen verschiedenen F�higkeiten
 * @author ALJETKE
 *
 */
public class Raumschiff {

	private int AnzahlTorpedoes;
	private int energieInProzent;
	private int huelleInProzent;
	private int schildeInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int anzahlAndroiden;
	private String raumschiffName;
	private static ArrayList<String> broadCastCommunicator = new ArrayList<String>();
	private ArrayList<String> erhalteLogbucheintr�ge;
	private ArrayList<Ladung> ladungRaumschiff;
	
	/**
	 * der Standard Kontruktor
	 */
	public Raumschiff() {
		
	}
	
	/**
	 * der parametisierte Konstruktor
	 * @param anzahlTorpedoes - Munition an Board
	 * @param energieInProzent - prozentuale Energiemenge
	 * @param huelleInProzent- prozentuale H�llenmenge
	 * @param schildeInProzent- prozentuale Schildmenge
	 * @param lebenserhaltungssystemeInProzent - prozentuale lebenserhaltene Systemmenge
	 * @param anzahlAndroiden - Anzahl der Androiden auf dem Raumschiff
	 * @param raumschiffName - der Name des Raumschiffs
	 */
	public Raumschiff(int anzahlTorpedoes, int energieInProzent, int huelleInProzent, int schildeInProzent,
			int lebenserhaltungssystemeInProzent, int anzahlAndroiden, String raumschiffName) {

		setAnzahlTorpedoes(anzahlTorpedoes);
		setEnergieInProzent(energieInProzent);
		setHuelleInProzent(huelleInProzent);
		setSchildeInProzent(schildeInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAnzahlAndroiden(anzahlAndroiden);
		setRaumschiffName(raumschiffName);
		this.erhalteLogbucheintr�ge = new ArrayList<String>();
		this.ladungRaumschiff = new ArrayList<Ladung>();
	}
	/**
	 * gibt die Anzahl der Photonentorpedoes zur�ck
	 * @return
	 */
	public int getAnzahlTorpedoes() {
		return AnzahlTorpedoes;
	}
	/**
	 * setzt die Anzahl der Photonentorpedoes mit dem Parameter gleich
	 * @param anzahlTorpedoes - Munition im Schiff
	 */
	public void setAnzahlTorpedoes(int anzahlTorpedoes) {
		AnzahlTorpedoes = anzahlTorpedoes;
	}
	/**
	 * gibt die Energie des Raumschiffs aus, in Prozent
	 * @return
	 */
	public int getEnergieInProzent() {
		return energieInProzent;
	}
	/**
	 * setzt die Anzahl der Energie mit dem Parameter gleich
	 * @param energieInProzent - Energiemenge
	 */
	public void setEnergieInProzent(int energieInProzent) {
		this.energieInProzent = energieInProzent;
	}
	/**
	 * gibt die prozentuale menge der H�lle aus
	 * @return
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	/**
	 * setzt die Prozent der H�lle mit dem Parameter gleich
	 * @param huelleInProzent - H�llenmenge
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	/**
	 * gibt die prozentuale menge an Schilde aus
	 * @return
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	/**
	 * setzt die Prozent der Schilde mit dem Parameter gleich
	 * @param schildeInProzent - Schildmenge
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	/**
	 * gibt die Prozent der Lebenserhaltenen Systeme aus
	 * @return
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	/**
	 * setzt die Prozent der Lebenserhaltenen Systemen mit dem Parameter gleich
	 * @param lebenserhaltungssystemeInProzent - lebenserhaltene Systemmenge
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	/**
	 * gibt die Anzahl der Androiden auf dem Raumschiff zur�ck
	 * @return
	 */
	public int getAnzahlAndroiden() {
		return anzahlAndroiden;
	}
	/**
	 * setzt die Anzahl der Androiden mit dem Parameter gleich
	 * @param anzahlAndroiden - Anzahl der Androiden aufm Raumschiff
	 */
	public void setAnzahlAndroiden(int anzahlAndroiden) {
		this.anzahlAndroiden = anzahlAndroiden;
	}
	/**
	 * gibt den Raumschiffnamen zur�ck
	 * @return
	 */
	public String getRaumschiffName() {
		return raumschiffName;
	}
	/**
	 * setzt den Raumschiffsnamen mit dem Parameter gleich
	 * @param raumschiffName - Name des Raumschiffs
	 */
	public void setRaumschiffName(String raumschiffName) {
		this.raumschiffName = raumschiffName;
	}
	/**
	 * gibt die Ladung des Raumschiffes zur�ck
	 * @return
	 */
	public ArrayList<Ladung> getLadungRaumschiff() {
		return ladungRaumschiff;
	}
	/**
	 * f�gt die Ladung des Raumschiffes mit dem Parameter hinzu
	 * @param ladungRaumschiff - Ladung die sich auf dem Raumschiff befindet
	 */
	public void setLadungRaumschiff(Ladung ladungRaumschiff) {
		this.ladungRaumschiff.add(ladungRaumschiff);
	}

	/**
	 * schie�en der Photonentorpedos von diesem Raumschiff auf das Raumschiff das �bergeben wurde
	 * @param schiff - Raumschiff auf das geschossen wird
	 */
	public void feuerTorpedoes(Raumschiff schiff) {
		if (this.AnzahlTorpedoes > 0) {
			System.out.println(this.raumschiffName + " Photonentorpedos abgeschossen");
			treffer(schiff);
			this.AnzahlTorpedoes -= 1;
		} else {
			System.out.println("-=*Click*=-");
		}
		System.out.println();
	}
	/**
	 * schie�en der Phaser von diesem Raumschiff auf das Raumschiff das �bergeben wurde
	 * @param schiff - Raumschiff auf das geschossen wird
	 */
	public void feuerPhaser(Raumschiff schiff) {
		if (this.energieInProzent > 49) {
			System.out.println(this.raumschiffName + " Phaser abgeschossen");
			treffer(schiff);
			this.energieInProzent -= 50;
		} else {
			System.out.println("-=*Click*=-");
		}
		System.out.println();
	}
	/**
	 * das verzeichnen eines Treffers bei dem im Parameter �bergebenem Schiff
	 * @param schiff - zu treffende Raumschiff
	 */
	private void treffer(Raumschiff schiff) {
		System.out.println("" + schiff.raumschiffName + " wurde getroffen!");
		if (schiff.schildeInProzent > 49) {
			schiff.schildeInProzent -= 50;
		} else {
			schiff.huelleInProzent -= 50;
			schiff.energieInProzent -= 50;
			if (schiff.huelleInProzent < 0) {
				System.out.println("" + schiff.raumschiffName + "s Lebenserhaltene Systeme wurden zerst�rt!");
				schiff.lebenserhaltungssystemeInProzent = 0;
			}
		}
		System.out.println();

	}
	/**
	 * versendet eine Nachricht an alle
	 * @param nachricht - die Nachricht die ausgegeben wird
	 */
	public void nachrichtAnAlle(String nachricht) {
		System.out.println("" + nachricht + "  wurde im broadcastKommunikator hinzugef�gt!");
		broadCastCommunicator.add(nachricht);
		System.out.println();
	}
	/**
	 * die eintr�ge die im Lobuch vorgenommen wurden ausgeben
	 * @return logbuch - gibt alle Nachrichten zur�ck
	 */
	public ArrayList<String> eintraegeLogbuch() {
		return this.erhalteLogbucheintr�ge;
	}
	/**
	 * die Funktion zum reparieren eines Raumschiffs und welche Teile repariert werden sollen, sowie wieviele Andoriden verwendet werden sollen 
	 * @param schilde zu reparieren?
	 * @param huelle zu reparieren?
	 * @param energie zu reparieren?
	 * @param androidenAnzahl zu reparieren?
	 */
	public void repariereRaumschiff(Boolean schilde, Boolean huelle, Boolean energie, int androidenAnzahl) {
		if (androidenAnzahl <= this.anzahlAndroiden) {
			System.out.println(this.raumschiffName + " repariert sich.");
			int zufall;
			int reperaturwert;
			zufall = (int) (Math.random() * 100);
			if (schilde & huelle & energie) {

				reperaturwert = zufall * androidenAnzahl / 3;
				this.huelleInProzent += reperaturwert;
				this.schildeInProzent += reperaturwert;
				this.energieInProzent += reperaturwert;
			} else if (schilde & huelle) {
				reperaturwert = zufall * androidenAnzahl / 2;
				this.huelleInProzent += reperaturwert;
				this.schildeInProzent += reperaturwert;

			} else if (huelle & energie) {
				reperaturwert = zufall * androidenAnzahl / 2;
				this.huelleInProzent += reperaturwert;
				this.energieInProzent += reperaturwert;
			} else if (huelle & energie) {
				reperaturwert = zufall * androidenAnzahl / 2;
				this.huelleInProzent += reperaturwert;
				this.energieInProzent += reperaturwert;
			} else if (huelle) {
				reperaturwert = zufall * androidenAnzahl;
				this.huelleInProzent += reperaturwert;
			} else if (energie) {
				reperaturwert = zufall * androidenAnzahl;
				this.energieInProzent += reperaturwert;
			} else if (schilde) {
				reperaturwert = zufall * androidenAnzahl;
				this.schildeInProzent += reperaturwert;

			}
		} else {
			System.out.println(" Nicht genug Reperaturandroiden vorhanden!");
		}
		System.out.println();
	}
	/**
	 * gibt auf der Console aus wie der Status des Raumschiffs ist
	 */
	public void statusRaumschiff() {
		System.out.println("Raumschiffname:" + this.raumschiffName);
		System.out.println("Energie:" + this.energieInProzent + "%");
		System.out.println("H�lle:" + this.huelleInProzent + "%");
		System.out.println("Schilde:" + this.schildeInProzent + "%");
		System.out.println("Lebenserhaltene Systeme:" + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Torpedoes:" + this.AnzahlTorpedoes);
		System.out.println("Androiden:" + this.anzahlAndroiden);
		System.out.println();
	}
	/**
	 * gibt auf der Console den Status der Ladung f�r dieses Raumschiff aus
	 */
	public void statusLadung() {
		System.out.println("" + this.raumschiffName + " enth�lt:");
		for (int i = 0; i < this.ladungRaumschiff.size(); i++)
			System.out.println(this.ladungRaumschiff.get(i).toString());
		System.out.println();
	}
	/**
	 * leert eine Ladung aus dem raumschiff
	 * @param item - die Ladung die entleert werden soll 
	 */
	public void leereLadung(Ladung item) {
		System.out.println(this.raumschiffName +" leert:"+ item);
		item.leereLadung();
		System.out.println();
	}
}
